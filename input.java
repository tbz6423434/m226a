import java.util.Scanner;
import java.util.ArrayList;

public class input {
  public static void main(String[] args) {
    Scanner myObj = new Scanner(System.in);
    int i = 0;
    String userInput;
    ArrayList<String> items = new ArrayList<String>();
    while (i < 10){
        System.out.println("Add an item or write Stop:");
        userInput = myObj.nextLine();
        if (userInput.equals("Stop")) {
            break;
        } else {
            items.add(userInput);
            i++;
        }
    }
    if (items.size() == 0) {
        System.out.println("There are no items.");
    } else {
        System.out.println("Your items are: " + items);
    }
  }
}
